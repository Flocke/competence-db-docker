# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-26 20:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0002_auto_20170219_2052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='competences',
            field=models.TextField(),
        ),
    ]

from django.shortcuts import HttpResponse, HttpResponseRedirect, render
from . import forms
# from django.core.validators import validate_email
# from django.core.exceptions import ValidationError


def index(request):
    return render(request, 'database/index.html')
    # return HttpResponse("Hello, you've reached the index of IOG Competence DB")


def register(request):
    if request.method == 'POST':
        form = forms.MemberRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/index/register_success')
        else:
            return HttpResponseRedirect('/index')
    return render(request, 'database/register.html')


def register_success(request):
    response = "The Registration was successful, your details are now listed " \
               "in the IOG Austria Competence Database."
    return HttpResponse(response)


def member(request, member_id):
    response = "You're looking at the member with id={id}".format(id=member_id)
    return HttpResponse(response)


# def regional_group(request, regional_group_id):
#     response = "You're looking at the regional group with " \
#                "id={id}".format(id=regional_group_id)
#     return HttpResponse(response)
#
#
# def focus_group(request, focus_group_id):
#     response = "You're looking at the focus group with " \
#                "id={id}".format(id=focus_group_id)
#     return HttpResponse(response)


def competence_search(request):
    return HttpResponse("Competence Search Page for the IOG Competence DB")

# TODO include get_object_or_404 and render!

# def eingabemaske(request):
#     return render(request, 'Eingabemaske.html')
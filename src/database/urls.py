from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register$', views.register, name='register'),
    url(r'^search$', views.competence_search, name='search'),
    url(r'^(?P<member_id>[0-9]+)/$', views.member, name='member'),
    url(r'^register_success$', views.register_success, name='register success'),
    # url(r'^eingabemaske', views.eingabemaske, name='eingabemaske'),

    # url(r'^(?P<regional_group_id>[0-9]+)/$', views.regional_group,
    #     name='regional group'),
    # url(r'^(?P<focus_group_id>[0-9]+)/$', views.focus_group,
    #     name='focus group'),
]